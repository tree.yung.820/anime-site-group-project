import express, { Request, Response } from "express";
import { client } from "../lib/db";
import { createResultJSON } from "../lib/helper";

const epHistoryRoutes = express.Router();

epHistoryRoutes.post("/ep-history/:epId", insertEpHistory);

async function insertEpHistory(req: Request, res: Response) {
	const epId = Number(req.params.epId);
	let userId = null;

	if (req.session["user"]) {
		userId = req.session["user"].id;
	}

	await client.query(
		/*sql*/ `INSERT INTO episode_history (user_id, episode_id, created_at)
        VALUES ($1, $2, NOW())`,
		[userId, epId]
	);

    res.json(createResultJSON({message: "episode_history record created."}))
}

export default epHistoryRoutes;
