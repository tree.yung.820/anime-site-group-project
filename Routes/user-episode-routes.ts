import express, { Request, Response } from "express";
import { client } from "../lib/db";
import { isLoggedIn } from "../lib/guard";
import { camelCaseKeys } from "../lib/helper";
import { LikeStatus, WatchInfo } from "../lib/interface";

const userEpisodeRoute = express.Router();
userEpisodeRoute.get("/user-ep/:epId", getUserStatus);
userEpisodeRoute.put("/user-ep", isLoggedIn, updateUserEpisode);
userEpisodeRoute.get("/ep/:epId/like", getLikeStatus);

async function getUserStatus(req: Request, res: Response) {
	let userStatus = { startTime: 0, likeStatus: "" };

	if (req.session["user"]) {
		const epId: number = Number(req.params.epId);
		const userId: number = req.session["user"].id;
		let queryResult = await queryUserEpisode(userId, epId);

		if (queryResult) {
			userStatus.likeStatus = queryResult.likeStatus;
			userStatus.startTime = queryResult.lastWatched;
		} else {
			await client.query(
				/*sql*/ `INSERT INTO user_episode (user_id, episode_id, created_at)
				VALUES ($1, $2, NOW())`,
				[userId, epId]
			);
		}
	}

	res.json(userStatus);
}

async function updateUserEpisode(req: Request, res: Response) {
	const userId: number = req.session["user"].id;
	const { epId, time, likeStatus }: WatchInfo = req.body;

	await client.query(
		/*sql*/ `UPDATE user_episode SET last_watched = $1, like_status = $2, updated_at = NOW()
			WHERE user_id = $3 AND episode_id = $4`,
		[time, likeStatus, userId, epId]
	);

	res.json();
}

async function getLikeStatus(req: Request, res: Response) {
	const epId = Number(req.params.epId);

	let queryResult = camelCaseKeys(
		(
			await client.query(
				/*sql*/ `SELECT like_status, count(like_status) FROM user_episode 
			WHERE episode_id = $1 GROUP BY like_status`,
				[epId]
			)
		).rows
	);

	let epLikeStatus = queryResult.reduce(
		(e: LikeStatus, { likeStatus, count }: { likeStatus: number; count: number }) => ((e[likeStatus] = count), e),
		{}
	);

	res.json(epLikeStatus);
}

async function queryUserEpisode(userId: number, epId: number) {
	return camelCaseKeys(
		(
			await client.query(
				/*sql*/ `SELECT last_watched, like_status FROM user_episode 
			WHERE user_id = $1 AND episode_id = $2`,
				[userId, epId]
			)
		).rows[0]
	);
}

export default userEpisodeRoute;
