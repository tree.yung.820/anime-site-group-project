import express, { Request, Response } from "express";
import { client } from "../lib/db";
import { camelCaseKeys, createResultJSON } from "../lib/helper";
import { queryFavoriteList, insertFavoriteList } from "./favorite-routes";

const episodeRoute = express.Router();

episodeRoute.get("/series/:seriesId/ep/:epId", getEpisodes);

async function getEpisodes(req: Request, res: Response) {
	const seriesId: number = Number(req.params.seriesId);
	console.log("series id is ",seriesId) //added
	let currentEpId: number = Number(req.params.epId);
	console.log("current EPid  is ", currentEpId) //added
	const episodes = await queryEpisodes(seriesId);

	if (!currentEpId) {
		currentEpId = await queryFirstEpId(seriesId);

		if (req.session["user"]) {
			const userId = req.session["user"].id;
			const queryResult = await queryFavoriteList(userId, seriesId);

			queryResult
				? (currentEpId = queryResult.episodeId)
				: await insertFavoriteList(userId, seriesId, currentEpId);
		}
	}

	if (episodes.length && currentEpId) {
		res.json({ episodes: episodes, currentEpId: currentEpId });
	} else {
		res.status(404).json(createResultJSON(null, { message: "episode not found." }));
	}
}

async function queryEpisodes(seriesId: number) {
	const queryResult = (
		await client.query(
			/*sql*/ `SELECT id, series_id, episode_number, title, local_path, external_link
			FROM episodes 
			WHERE series_id = $1 ORDER BY episode_number`,
			[seriesId]
		)
	).rows;

	return camelCaseKeys(queryResult);
}

async function queryFirstEpId(seriesId: number) {
	return (await client.query(/*sql*/ `SELECT id FROM episodes WHERE series_id = $1`, [seriesId])).rows[0].id;
}

export default episodeRoute;
