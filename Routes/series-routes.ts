import express, { Request, Response } from "express";
import { client } from "../lib/db";
import { createResultJSON } from "../lib/helper";
import { SeriesData } from "../lib/interface";

const seriesRoutes = express.Router();

seriesRoutes.get("/series", getSeries);
seriesRoutes.get("/week-series", getWeekSeries);
seriesRoutes.get("/season-series", getSeasonSeries);
seriesRoutes.get("/series/:id", getOneSeries);

async function getSeries(req: Request, res: Response) {
	let series = (await client.query(/*sql */
		`SELECT s.title , s.picture_path , s.id 
	from series as s
	WHERE s.created_at > CURRENT_DATE - INTERVAL '7 days'
	order by s.created_at desc;`));
	res.json(series.rows);
}
async function getWeekSeries(req: Request, res: Response) {
	let result = await client.query(/*sql */
		`SELECT s.title , s.picture_path , s.id,count(*) 
	FROM episode_history
	join episodes on episode_history.episode_id = episodes.id 
	join series as s on episodes.series_id = s.id
	WHERE episode_history.created_at > CURRENT_DATE - INTERVAL '7 days'
	group by s.id 
	order by count(*) desc 
	limit 5;`);
	res.json(result.rows);
}
async function getSeasonSeries(req: Request, res: Response) {
	let result = await client.query(/*sql */`SELECT s.title , s.picture_path , s.id,count(*) 
	FROM episode_history
	join episodes on episode_history.episode_id = episodes.id 
	join series as s on episodes.series_id = s.id
	WHERE episode_history.created_at > CURRENT_DATE - INTERVAL '90 days'
	group by s.id 
	order by count(*) desc 
	limit 5;`);
	res.json(result.rows);
}

async function getOneSeries(req: Request, res: Response) {
	const seriesId = Number(req.params.id);
	const seriesInfo: Omit<SeriesData, "updateOn" | "picturePath"> = (
		await client.query(/*sql*/ `SELECT title, description FROM series WHERE id = $1`, [seriesId])
	).rows[0];

	if (seriesInfo) {
		res.json(seriesInfo);
	} else {
		res.status(404).json(createResultJSON(null, { message: "series not found." }));
	}
}

export default seriesRoutes;
