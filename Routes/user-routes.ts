import express, { Request, Response } from "express";
import fetch from "node-fetch";
import { client } from "../lib/db";
import { isLoggedIn } from "../lib/guard";
import { checkPassword, hashPassword } from "../lib/hash";
import { camelCaseKeys, createResultJSON } from "../lib/helper";
import { logger } from "../lib/logger";
import { multerSingleImage } from "../lib/multer";

let defaultAvatar = "/pictures/guest.png";

const userRoutes = express.Router();

userRoutes.post("/login", login);
userRoutes.post("/validate", validateUser);
userRoutes.post("/register", multerSingleImage, createUser);
userRoutes.get("/login/google", loginGoogle);
userRoutes.get("/user", getUser).put("/user", isLoggedIn, multerSingleImage, updateUser);
userRoutes.get("/user/history", isLoggedIn, getUserData);
userRoutes.post("/mode", postTheme);
userRoutes.post("/logout", isLoggedIn, logout);

async function login(req: Request, res: Response) {
    const { username, password } = req.body;
    const userResult = await client.query(`SELECT * FROM users WHERE username = $1`, [username]);
    const foundUser = userResult.rows[0];

    if (!foundUser) {
        res.status(400).json(createResultJSON(null, { message: "username or password not correct." }));
        return;
    }

    let isPasswordValid = await checkPassword(password, foundUser.password);

    if (!isPasswordValid) {
        delete req.session["user"];
        res.status(400).json(createResultJSON(null, { message: "username or password not correct." }));
        return;
    }

    req.session["user"] = foundUser;
    res.json(createResultJSON({ message: "login success" }));
}

async function validateUser(req: Request, res: Response) {
    logger.debug(`req.body = ${JSON.stringify(req.body, null, 2)}`);
    const { email, username, password, confirmPassword } = req.body;

    console.log("4 object", email, username, password, confirmPassword);

    if (email != "") {
        let emailResult = await client.query(/*sql*/ `SELECT * FROM users WHERE email = $1`, [email]);

        if (emailResult.rows.length) {
            res.status(400).json(createResultJSON(null, { message: "你個email有人用咗喇。" }));
            return;
        }
    }
	if (!username.trim() || !password.trim() || !email.split("@")[0].trim()) {
		res.status(400).json(createResultJSON(null, {message: "唔可以齋用空格㗎。"}))
		return;
	}

    let userResult = await client.query(/*sql*/ `SELECT * FROM users WHERE username = $1`, [username]);

    if (userResult.rows.length) {
        res.status(400).json(createResultJSON(null, { message: "你個花名俾人用咗喇。" }));
        return;
    }

    if (password && password !== confirmPassword) {
        res.status(400).json(createResultJSON(null, { message: "你打錯密碼喎。" }));
        return;
    }

    res.json(createResultJSON({ message: "validation success" }));
}

async function createUser(req: Request, res: Response) {
    logger.debug(`create user req.body = ${JSON.stringify(req.body, null, 2)}`);
    const { email, username, password } = req.body;

    let filename = req.file ? req.file.filename : defaultAvatar;
    let hashedPassword = await hashPassword(password);

    let user = (
        await client.query(
            /*sql*/
            `INSERT INTO users (username, email, password, avatar_path, created_at) 
        values($1, $2, $3, $4, NOW()) returning id, username`,
            [username, email, hashedPassword, filename]
        )
    ).rows[0];

    req.session["user"] = user;
    console.log(user);

    logger.debug("user created");
    res.json(createResultJSON({ message: "user created" }));
}

async function loginGoogle(req: Request, res: Response) {
    const accessToken = req.session?.["grant"].response.access_token;
    logger.debug(`accessToken = ${accessToken}`);

    const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
        method: "get",
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    });

    const result = await fetchRes.json();
    logger.debug(`result = ${JSON.stringify(result, null, 2)}`);

    let users = (await client.query(`SELECT * FROM users WHERE email = $1`, [result.email])).rows;
    logger.debug(`users = ${JSON.stringify(users, null, 2)}`);

    if (!users.length) {
        let { email, name } = result;
        await client.query(
            /*sql*/
            `INSERT INTO users (username, email, created_at, updated_at) 
			values($1, $2, NOW(), NOW())`,
            [name, email]
        );
        users = (await client.query(`SELECT * FROM users WHERE email = $1`, [result.email])).rows;
    }
    let user = users[0];
    if (req.session) {
        req.session["user"] = user;
    }

    res.redirect("/index.html");
}

//user info
async function getUser(req: Request, res: Response) {
    let user = req.session["user"];
    let mode = req.session["themeMode"];

    if (req.session["themeMode"] === undefined) {
        mode = true;
    }

    if (user) {
        let userResult = camelCaseKeys((await client.query(/*sql*/ `SELECT id, username, avatar_path, is_admin FROM users WHERE id = $1`, [user.id])).rows[0]);

        res.json({ userInfo: userResult, mode: mode });
    } else {
        res.status(400).json({ message: "no user found", mode: mode });
    }
}

async function updateUser(req: Request, res: Response) {
    // logger.debug(`update user req.body = ${JSON.stringify(req.body, null, 2)}`);
    let { username, password } = req.body;

    console.log(req.body);

    const userId = req.session["user"].id;
    let filename = req.file ? req.file.filename : null;
    let hashedPassword = password ? await hashPassword(password) : null;
    !username && (username = null);

    await client.query(
        /*sql*/
        `update users set username = COALESCE($1, username), password = COALESCE($2, password), avatar_path = COALESCE($3, avatar_path)
		where id = $4;`,
        [username, hashedPassword, filename, userId]
    );

    logger.debug("user info changed");
    res.json(createResultJSON({ message: "user info changed." }));
}

// search history
async function getUserData(req: Request, res: Response) {
    let user = req.session["user"];
    let historyResult = await client.query(
        /*sql*/ `SELECT user_id, series.title, series.id, episodes.episode_number , series.picture_path, bookmark, favorite_lists.updated_at 
		FROM favorite_lists join series on series_id= public.series.id join episodes on favorite_lists.episode_id = episodes.id WHERE user_id = $1`,
        [user.id]
    );
    res.json(historyResult);
}

async function logout(req: Request, res: Response) {
    delete req.session["user"];
    res.json(createResultJSON({ message: "logout" }));
}

function postTheme(req: Request, res: Response) {
    let themeMode = req.body.lightMode;

    req.session["themeMode"] = themeMode;
    res.json();
}

export default userRoutes;
