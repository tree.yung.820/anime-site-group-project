import express, { Request, Response } from "express";
import { client } from "../lib/db";
import { isLoggedIn } from "../lib/guard";
import { camelCaseKeys } from "../lib/helper";

const favoriteRoutes = express.Router();

favoriteRoutes.get("/favorite/:seriesId", getBookmark);
favoriteRoutes.put("/favorite", isLoggedIn, updateFavoriteList);

async function updateFavoriteList(req: Request, res: Response) {
    console.log("updateFavoriteList~");

    let userId = req.session["user"].id;
    let { seriesId, epId, bookmark } = req.body;
    console.log(req.body); //added
    await client.query(
        /*sql*/ `UPDATE favorite_lists SET episode_id = $1, bookmark = $2, updated_at = NOW() 
			WHERE user_id = $3 AND series_id = $4`,
        [epId, bookmark, userId, seriesId]
    );
}

async function getBookmark(req: Request, res: Response) {
    let bookmark: boolean = false;

    if (req.session["user"]) {
        let userId: number = req.session["user"].id;
        let seriesId: number = Number(req.params.seriesId);
        let queryResult = await queryFavoriteList(userId, seriesId);
        console.log("result", queryResult);

        if (queryResult.bookmark) {
            bookmark = true;
        }
    }

    res.json(bookmark);
}

export async function queryFavoriteList(userId: number, seriesId: number) {
    const queryResult = (
        await client.query(
            /*sql*/ `SELECT episode_id, bookmark
			FROM favorite_lists
			WHERE user_id = $1 AND series_id = $2`,
            [userId, seriesId]
        )
    ).rows[0];

    return camelCaseKeys(queryResult);
}

export async function insertFavoriteList(userId: number, seriesId: number, epId: number) {
    await client.query(
        /*sql*/ `INSERT INTO favorite_lists (user_id, series_id, episode_id, created_at, updated_at)
	VALUES ($1, $2, $3, NOW(), NOW())`,
        [userId, seriesId, epId]
    );
}

export default favoriteRoutes;
