import express, { Request, Response } from "express";
import { client } from "../lib/db";
import { isLoggedIn } from "../lib/guard";
import { camelCaseKeys } from "../lib/helper";

const commentRoutes = express.Router();

commentRoutes.route("/comment/:seriesId").get(getComments).post(isLoggedIn, postComments);

async function getComments(req: Request, res: Response) {
	const seriesId = Number(req.params.seriesId);
	const comments = await queryComments(seriesId);

	res.json(comments);
}

async function postComments(req: Request, res: Response) {
	const seriesId: number = Number(req.params.seriesId);
	const userId: number = req.session["user"].id;
	const { epId, content } = req.body;

	await client.query(
		/*sql*/ `INSERT INTO comments (user_id, series_id, episode_id, content, created_at, updated_at)
        VALUES($1, $2, $3, $4, NOW(), NOW())`,
		[userId, seriesId, epId, content]
	);
	res.json();
}

async function queryComments(seriesId: number) {
	return camelCaseKeys(
		(
			await client.query(
				/*sql*/
				`SELECT users.username as username, episodes.episode_number , content, users.avatar_path as avatar, comments.created_at
                FROM comments join users on user_id = users.id join episodes on episode_id = episodes.id
				where comments.series_id = $1 order by comments.created_at DESC`,
				[seriesId]
			)
		).rows
	);
}

export default commentRoutes;
