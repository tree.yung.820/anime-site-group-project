import express, { Request, Response } from "express";
import { client } from "../lib/db";
import { createResultJSON } from "../lib/helper";
import { io } from "../lib/socket-io";

const danmakuRoutes = express.Router();

danmakuRoutes.route("/danmaku/:episodeId").get(getDanmaku).post(postDanmaku);
danmakuRoutes.route("/danmaku").get(getDanmakuStatus).post(postDanmakuStatus);

async function getDanmaku(req: Request, res: Response) {
	const episodeId: number = Number(req.params.episodeId);
	const queryResult = (
		await client.query(
			/*sql*/ `SELECT content, mode, color, time FROM danmakus WHERE episode_id = $1 ORDER BY time`,
			[episodeId]
		)
	).rows;
	const danmakuArr = queryResult.map(makeDanmaku);

	res.json(danmakuArr);
}

async function postDanmaku(req: Request, res: Response) {
	const userId: number = req.session["user"].id;
	const epId: number = Number(req.params.episodeId);
	const danmaku = { ...req.body };
	const { text, mode, time, style } = danmaku;
	const { color } = style;

	if (text.trim()) {
		await client.query(
			/*sql*/
			`INSERT INTO danmakus (user_id, episode_id, content, mode, color, time, created_at) 
			VALUES ($1, $2, $3, $4, $5, $6, NOW())`,
			[userId, epId, text, mode, color, time]
		);

		io.to(epId.toString()).emit("danmaku", danmaku);
		res.json(createResultJSON({ message: "danmaku posted." }));
	} else {
		res.json(createResultJSON(null, { message: "no danmaku text." }));
	}
}

function makeDanmaku(danmaku: any) {
	return {
		text: danmaku.content,
		mode: danmaku.mode,
		time: danmaku.time,
		style: {
			fontSize: "20px",
			color: danmaku.color,
			textShadow: "-1px -1px #000, -1px 1px #000, 1px -1px #000, 1px 1px #000",
		},
	};
}

export default danmakuRoutes;
function getDanmakuStatus(req: Request, res: Response) {
	let status: boolean = req.session["danmaku"] == undefined ? true : req.session["danmaku"];
	console.log("danmaku: ", status);
	
	res.json({ status: status });
}

function postDanmakuStatus(req: Request, res: Response) {
	let { status } = req.body;
	console.log("body", req.body);
	
	req.session["danmaku"] = status
	console.log("session", status);
	
	res.json()
}
