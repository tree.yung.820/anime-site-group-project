export async function updateFavoriteList(seriesId) {
	const activeIcon = "fas";
	const epId = Number(document.querySelector(".episode-title").id);
	const bookmarkBtn = document.querySelector("#bookmark-button");
	const isBookmarkBtnPressed = bookmarkBtn.querySelector("i").classList.contains(activeIcon);
	let favorite = {
		seriesId: seriesId,
		epId: epId,
		bookmark: isBookmarkBtnPressed,
	};
	await fetch(`/favorite`, {
		method: "put",
		headers: {
			"Content-type": "application/json; charset=utf-8",
		},
		body: JSON.stringify(favorite),
	});
}
