export function errorMessage(result) {
	let errorElem = document.querySelector(".error");
	errorElem.innerHTML = "";
	let errorMessage = result.err.message;
	errorElem.innerHTML = errorMessage;
}

export function previewPic(e, originalPic) {
	let avatarPreview = document.querySelector("#avatar-preview");
	let file = e.target.files[0];

	avatarPreview.src = file ? URL.createObjectURL(file) : originalPic;
}

export function convertToLocalTime(timeString) {
	let date = new Date(timeString).toLocaleDateString();
	if (date.includes("/")) {
		date = date.split("/").reverse().join("-");
	}
	let time = new Date(timeString).toLocaleTimeString();

	return date + " " + time;
}

export function convertToLocalDate(timeString) {
	let date = new Date(timeString).toLocaleDateString();
	if (date.includes("/")) {
		date = date.split("/").reverse().join("-");
	}
	return date;
}

export function changeActiveStatus(e, oppBtn) {
	const activeIcon = "fas";
	let btnIcon = e.currentTarget.querySelector("i");
	let btnCount = e.currentTarget.querySelector("span");

	if (btnIcon.classList.contains(activeIcon)) {
		addInactiveIcon(btnIcon);
		btnCount && btnCount.innerHTML--;
	} else {
		addActiveIcon(btnIcon);
		btnCount && btnCount.innerHTML++;

		if (oppBtn) {
			let oppBtnIcon = oppBtn.querySelector("i");
			let oppBtnCount = oppBtn.querySelector("span");
			if (oppBtnIcon.classList.contains(activeIcon)) {
				addInactiveIcon(oppBtnIcon);
				oppBtnCount.innerHTML--;
			}
		}
	}
}

export function addActiveIcon(icon) {
	const activeIcon = "fas";
	const inactiveIcon = "far";
	icon.classList.add(activeIcon);
	icon.classList.remove(inactiveIcon);
}

export function addInactiveIcon(icon) {
	const activeIcon = "fas";
	const inactiveIcon = "far";
	icon.classList.add(inactiveIcon);
	icon.classList.remove(activeIcon);
}

export function showMessage(e) {
	let errorElem = document.querySelector(".error");
	if (errorElem.classList.contains("inactive")) {
		errorElem.classList.remove("inactive")
		let nameElem = document.querySelector("#name");
		let id = e.currentTarget.id;
		let name = id.split("-")[0];
		let px = name == "like" ? 90 : name == "dislike" ? 130 : 165;
		nameElem.innerHTML = `${name}?`;
		errorElem.style.display = "block";
		errorElem.style.left = px + "px";
		setTimeout(() => {
			document.body.addEventListener(
				"click",
				() => {
					errorElem.style.display = "none";
					errorElem.classList.add("inactive");
				},
				{ once: true }
			);
		}, 1);
	}
}
