export async function getSeries(seriesId) {
	const seriesTitleElem = document.querySelector(".series-title");
	const descriptionElem = document.querySelector(".description");
	let seriesRes = await fetch(`/series/${seriesId}`);

	if (seriesRes.ok) {
		const { title, description } = await seriesRes.json();
		document.title = title + " | WAIFU";
		seriesTitleElem.innerHTML = title;
		descriptionElem.innerHTML = description;
	} else {
		console.log(result.err.message);
	}
}
