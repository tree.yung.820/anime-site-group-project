import { addActiveIcon, addInactiveIcon } from "./helper.js";

export async function updateUserEpisode() {
	const activeIcon = "fas";
	const videoElem = document.querySelector("video");
	const epId = document.querySelector(".episode-title").id;
	const isLikeBtnPressed = document.querySelector("#like-button > i").classList.contains(activeIcon);
	const isDislikeBtnPressed = document.querySelector("#dislike-button > i").classList.contains(activeIcon);
	const watchInfo = {
		epId: epId,
		time: Math.floor(videoElem.currentTime),
		likeStatus: isLikeBtnPressed ? "like" : isDislikeBtnPressed ? "dislike" : null,
	};
	
	await fetch(`/user-ep`, {
		method: "put",
		headers: {
			"Content-type": "application/json; charset=utf-8",
		},
		body: JSON.stringify(watchInfo),
	});
}

export async function loadLikeStatus(likeStatus, id) {
	const res = await fetch(`/ep/${id}/like`);
	const epLikeStatus = await res.json();
	const activeIcon = "fas";
	const inactiveIcon = "far";
	const likeBtnIcon = document.querySelector(".fa-thumbs-up");
	const dislikeBtnIcon = document.querySelector(".fa-thumbs-down");
	const likeCount = document.querySelector("#like");
	const dislikeCount = document.querySelector("#dislike");
	const { like, dislike } = epLikeStatus;

	likeStatus === "like" ? addActiveIcon(likeBtnIcon): addInactiveIcon(likeBtnIcon);
	likeStatus === "dislike" ? addActiveIcon(dislikeBtnIcon): addInactiveIcon(dislikeBtnIcon);
	likeCount.innerHTML = like ? like : 0;
	dislikeCount.innerHTML = dislike ? dislike : 0;
}
