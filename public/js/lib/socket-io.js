import { emitDanmaku } from "./danmaku.js";

export const socket = io.connect();

socket.on("danmaku", comment => emitDanmaku(comment));

socket.on("delete", msg => {
	document.querySelector(".danmaku-form").innerHTML = "";
});
