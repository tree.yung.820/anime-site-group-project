import { convertToLocalTime } from "./helper.js";

export async function getComment(seriesId) {
	const commentElem = document.querySelector(".comment-section");
	let res = await fetch(`/comment/${seriesId}`);
	let commentArr = await res.json();

	commentElem.innerHTML = commentArr.reduce(loadComments, "");
}

function loadComments(commentList, comment) {
	return (
		commentList +
		`<div class="comment-wrap">
            <img class="avatar" src="${comment.avatar}" alt="">
            <div class="comment">
            <span class="username">${comment.username}</span><span>第 ${comment.episodeNumber} 集</span>
			<span>${convertToLocalTime(comment.createdAt)}</span>
                <div class="comment-content">${comment.content}</div>
            </div>
        </div>`
	);
}

export async function postComment(e, seriesId) {
	e.preventDefault();
	const commentElem = document.querySelector(".comment-section");
	const commentCount = document.querySelector(".comment-count");
	const episodeTitleElem = document.querySelector(".episode-title");
	const epId = episodeTitleElem.id;
	let content = e.target.content;
	let comment = {
		epId: epId,
		content: content.value,
	};
	const res = await fetch(`/comment/${seriesId}`, {
		method: "POST",
		headers: { "Content-Type": "application/json; charset=utf-8" },
		body: JSON.stringify(comment),
	});

	if (res.ok) {
		content.value = "";
		commentCount.innerHTML = 0;
		let commentRes = await fetch(`/comment/${seriesId}`);
		let commentArr = await commentRes.json();
		commentElem.innerHTML = commentArr.reduce(loadComments, "");
	}
}

export function getCommentCount(e) {
	const wordCount = e.target.value.length;
	const commentCount = document.querySelector(".comment-count");

	commentCount.innerHTML = wordCount;
}
