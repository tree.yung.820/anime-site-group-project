import { socket } from "./socket-io.js";
import { loadDanmaku, resetDanmaku, resizeDanmaku } from "./danmaku.js";
import { updateUserEpisode, loadLikeStatus } from "./user-episode.js";

export async function getEpisodes(seriesId, episodeId) {
    episodeId = episodeId ? episodeId : "nan";
    const res = await fetch(`/series/${seriesId}/ep/${episodeId}`);
    console.log("the res value is :", res);

    if (res.ok) {
        const { episodes, currentEpId } = await res.json();
        const currentEpisode = episodes.filter((ep) => ep.id === currentEpId)[0];
        const otherEpisodes = episodes.filter((ep) => ep.id !== currentEpId);
        const deleteBtn = document.querySelector("#delete-btn");

        loadCurrentEpisode(currentEpisode);
        loadEpisodeList(otherEpisodes);
        deleteBtn.onclick = () => {
            socket.emit("delete-danmaku", currentEpId);
        };
    } else {
        let result = res.json();
        console.log(result.err.message);
    }
}

async function loadCurrentEpisode(currentEpisode) {
    const { id, episodeNumber, title, localPath } = currentEpisode;
    const res = await fetch(`/user-ep/${id}`);
    const { likeStatus, startTime } = await res.json();
    const episodeTitleElem = document.querySelector(".episode-title");

    socket.emit("join-room", id);
    loadLikeStatus(likeStatus, id);
    //get video and danmaku
    await loadDanmaku(id);
    loadVideo(localPath, startTime);
    //get description
    episodeTitleElem.id = id;
    episodeTitleElem.innerHTML = `第 ${episodeNumber} 集 — ${title}`;
    //insert episode_history record
    await fetch(`/ep-history/${id}`, { method: "POST" });
}

function loadVideo(path, startTime, id) {
    const videoElem = document.querySelector("video");
    const source = document.createElement("source");

    source.setAttribute("src", `/videos/${path}`);
    videoElem.innerHTML = "";
    videoElem.appendChild(source);
    videoElem.load();
    videoElem.onloadedmetadata = function () {
        this.currentTime = startTime;
        resizeDanmaku();
    };
}

function loadEpisodeList(otherEps) {
    const epListElem = document.querySelector(".ep-list");
    epListElem.innerHTML = "";
    epListElem.innerHTML = otherEps.reduceRight(getEpList, "");
}

function getEpList(ep, nextEp) {
    return (
        ep +
        `<li>
		<div class="ep-wrap" id="${nextEp.id}">
			<div class="ep-title">${nextEp.title}</div>
			<div class="ep-number">第 ${nextEp.episodeNumber} 集</div>
		</div>
	</li>`
    );
}

export async function loadEpisode(e, seriesId) {
    const episodeTitleElem = document.querySelector(".episode-title");
    const currentEpisodeId = episodeTitleElem.id;
    const nextEpisodeId = e.target.parentElement.id;

    if (nextEpisodeId) {
        resetDanmaku();
        socket.emit("leave-room", currentEpisodeId);
        updateUserEpisode();
        getEpisodes(seriesId, nextEpisodeId);
    }
}
