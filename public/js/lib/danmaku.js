let danmaku;

export async function loadDanmaku(epId) {
	const videoContainerElem = document.querySelector(".video-container");
	const videoElem = document.querySelector("video");
	const danmakuArr = await getDanmaku(epId);
	const danmakuElem = document.querySelector(".danmaku-content");
	const danmakuForm = document.querySelector(".danmaku-form");
	const danmakuStatus = document.querySelector("#danmaku-status");

	danmaku = new Danmaku({
		container: videoContainerElem,
		media: videoElem,
		comments: danmakuArr,
	});

	danmakuElem.innerHTML = danmakuArr.reduce(loadDanmakuList, "");
	danmakuForm.id = epId;
	!danmakuStatus.classList.contains("active") && danmaku.hide();
}

async function getDanmaku(epId) {
	const res = await fetch(`/danmaku/${epId}`);
	return await res.json();
}

function getTime(time) {
	const minute = Math.floor(time / 60)
		.toString()
		.padStart(2, "0");
	const second = Math.floor((time % 3600) % 60)
		.toString()
		.padStart(2, "0");
	return minute + ":" + second;
}

export async function postDanmaku(e) {
	e.preventDefault();
	const danmakuElem = document.querySelector(".danmaku-content");
	const videoElem = document.querySelector("video");
	const danmakuCount = document.querySelector(".danmaku-count");
	const danmakuColor = document.querySelector("#danmaku-color");
	const form = e.target;
	const epId = form.id;

	let formObj = {
		text: form.text.value,
		mode: getDanmakuMode(),
		time: videoElem.currentTime,
		style: {
			fontSize: "20px",
			color: danmakuColor.value,
			textShadow: "-1px -1px #000, -1px 1px #000, 1px -1px #000, 1px 1px #000",
		},
	};

	const res = await fetch(`/danmaku/${epId}`, {
		method: "POST",
		headers: { "Content-type": "application/json; charset=utf-8" },
		body: JSON.stringify(formObj),
	});

	if (res.ok) {
		form.text.value = "";
		danmakuCount.innerHTML = 0;
	}
}

export function getDanmakuCount(e) {
	const wordCount = e.target.value.length;
	const danmakuCountElem = document.querySelector(".danmaku-count");

	danmakuCountElem.innerHTML = wordCount;
}

export function emitDanmaku(comment) {
	let danmakuElem = document.querySelector(".danmaku-content");
	danmaku.emit(comment);
	danmakuElem.innerHTML = loadDanmakuList(danmakuElem.innerHTML, comment);
	console.log("after = ", danmakuElem.innerHTML);
}

export function changeDanmakuVis(e) {
	let elem = e.currentTarget;
	let elemClassList = e.currentTarget.classList;
	if (elemClassList.contains("active")) {
		deactivateDanmaku()
		danmaku.hide();
	} else {
		activateDanmaku()
		danmaku.show();
	}
}

export function resizeDanmaku() {
	danmaku.resize();
}

export function resetDanmaku() {
	danmaku.clear();
	danmaku.destroy();
}

export function changeDanmakuMode(e) {
	const danmakuMode = document.querySelectorAll(".danmaku-mode div");
	if (e.target.classList.contains("bi")) {
		Array.from(danmakuMode).map(e => e.classList.remove("active"));
		e.target.parentElement.classList.add("active");
	}
	getDanmakuMode();
}

function loadDanmakuList(danmaku, nextDanmaku) {
	return (
		danmaku +
		`<div class="danmaku-wrap">
			<span>${getTime(nextDanmaku.time)}</span>
			<span>${nextDanmaku.text}</span>
		</div>`
	);
}

export async function getDanmakuStatus() {
	const isDanmakuShow = await (await fetch("/danmaku")).json();
	isDanmakuShow.status ? activateDanmaku() : deactivateDanmaku();
}

export async function postDanmakuStatus() {
	const danmakuStatus = document.querySelector("#danmaku-status");
	const isDanmaku = danmakuStatus.classList.contains("active");
	await fetch(`/danmaku`, {
		method: "post",
		headers: {
			"Content-type": "application/json; charset=utf-8",
		},
		body: JSON.stringify({ status: isDanmaku }),
	});
}

function getDanmakuMode() {
	const danmakuMode = document.querySelectorAll(".danmaku-mode div");
	const mode = Array.from(danmakuMode).filter(e => e.classList.contains("active"))[0].id;
	return mode;
}

function activateDanmaku() {
	const danmakuStatus = document.querySelector("#danmaku-status");
	let statusClassList = danmakuStatus.classList;
	statusClassList.add("active");
	danmakuStatus.innerHTML = "開 啓";
}

function deactivateDanmaku() {
	const danmakuStatus = document.querySelector("#danmaku-status");
	let statusClassList = danmakuStatus.classList;
	statusClassList.remove("active");
	danmakuStatus.innerHTML = "關 閉";
}
