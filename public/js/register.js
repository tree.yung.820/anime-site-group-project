import { errorMessage, previewPic } from "./lib/helper.js";

let avatarPreview = document.querySelector("#avatar-preview");
let avatarInput = document.querySelector("#avatarInput");
let registerFormElem = document.querySelector("#register-form");
let originalPic = avatarPreview.src;

avatarInput.onchange = e=> previewPic(e, originalPic);
registerFormElem.addEventListener("submit", async e => {
	e.preventDefault();
	console.log("register clicked");
	let form = e.target;
	let formObj = {
		email: form.email.value,
		username: form.username.value,
		password: form.password.value,
		confirmPassword: form.confirmPassword.value,
	};

	let validateRes = await fetch("/validate", {
		method: "POST",
		headers: {
			"Content-Type": "application/json; charset=utf-8",
		},
		body: JSON.stringify(formObj),
	});

	if (validateRes.ok) {
		let formData = new FormData(form);
		let res = await fetch("/register", {
			method: "POST",
			body: formData,
		});

		if (res.ok) {
			window.location = "/index.html";
		}
	} else {
		let result = await validateRes.json();
		errorMessage(result);
	}
});