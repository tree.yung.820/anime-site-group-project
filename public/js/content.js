import "./common.js";
import { userInfo } from "./common.js";
import "./lib/socket-io.js";
import { getSeries } from "./lib/series.js";
import { getEpisodes, loadEpisode } from "./lib/episodes.js";
import { changeDanmakuMode, changeDanmakuVis, getDanmakuCount, postDanmaku, resizeDanmaku, getDanmakuStatus, postDanmakuStatus } from "./lib/danmaku.js";
import { updateUserEpisode } from "./lib/user-episode.js";
import { updateFavoriteList } from "./lib/favorite-lists.js";
import { changeActiveStatus, addActiveIcon, showMessage } from "./lib/helper.js";
import { getComment, postComment, getCommentCount } from "./lib/comment.js";

const params = new URLSearchParams(document.location.search);
const seriesId = params.get("id");

const epListElem = document.querySelector(".ep-list");
const danmakuForm = document.querySelector(".danmaku-form");
const danmakuStatus = document.querySelector("#danmaku-status");
const danmakuInput = document.querySelector(".danmaku-input");
const danmakuMode = document.querySelector(".danmaku-mode");
const likeBtn = document.querySelector("#like-button");
const dislikeBtn = document.querySelector("#dislike-button");
const bookmarkBtn = document.querySelector("#bookmark-button");
const commentForm = document.querySelector("#comment-form");
const commentInput = document.querySelector(".comment-input");

window.onload = async () => {
    await getSeries(seriesId);
    await getDanmakuStatus();
    await getEpisodes(seriesId);
    await getComment(seriesId);
    await getBookmark(seriesId);
    console.log("series id in content is", seriesId);
};

// other episodes
epListElem.onclick = (e) => loadEpisode(e, seriesId);

// danmaku
window.onresize = resizeDanmaku;
danmakuStatus.onclick = changeDanmakuVis;
danmakuForm.onsubmit = postDanmaku;
danmakuInput.oninput = getDanmakuCount;
danmakuMode.onclick = changeDanmakuMode;

// comment
commentForm.onsubmit = (e) => postComment(e, seriesId);
commentInput.oninput = getCommentCount;

// update user data
likeBtn.onclick = (e) => (userInfo.userInfo ? changeActiveStatus(e, dislikeBtn) : showMessage(e));
dislikeBtn.onclick = (e) => (userInfo.userInfo ? changeActiveStatus(e, likeBtn) : showMessage(e));
bookmarkBtn.onclick = (e) => (userInfo.userInfo ? changeActiveStatus(e) : showMessage(e));
document.addEventListener("visibilitychange", async (e) => {
    e.preventDefault();
    if (document.visibilityState === "hidden") {
    console.log("visibilitychange");

    await updateUserEpisode();
    await updateFavoriteList(seriesId);
    await postDanmakuStatus();
    }
});

async function getBookmark(seriesId) {
    let res = await fetch(`/favorite/${seriesId}`);
    if (res.ok) {
        const bookmarkIcon = document.querySelector("#bookmark-button > i");
        let isBookmarked = await res.json();

        isBookmarked && addActiveIcon(bookmarkIcon);
    }
}
