import { errorMessage } from "./lib/helper.js";

let loginFormElem = document.querySelector("#login-form");

loginFormElem.addEventListener("submit", async e => {
	e.preventDefault();
	console.log("log in clicked");

	const form = e.target;
	let formObj = {
		username: form.username.value,
		password: form.password.value,
	};

	const res = await fetch("/login", {
		method: "POST",
		headers: {"Content-type": "application/json; charset=utf-8"},
		body: JSON.stringify(formObj),
	});

	let result = await res.json();

	if (res.ok) {
		window.location = "/index.html";
	} else {
		errorMessage(result);
	}
});
