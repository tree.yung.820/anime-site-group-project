import "./common.js"


const updateSwiperWrapperElem = document.querySelector(".update-swiper-wrapper")
const weekSwiperWrapperElem = document.querySelector(".week-swiper-wrapper");
const seasonSwiperWrapperElem = document.querySelector(".season-swiper-wrapper");
let containerElem = document.querySelector(".container")
let swiperElem = document.querySelector(".swpier")


let updateGalleryList = []
let weekGalleryList = []
let seasonGalleryList = []


//main to run swiper functions
async function main() {
    await updateSwiper()
    await weekSwiper()
    await seasonSwiper()
}



//make update swiper
async function updateSwiper() {
    await getUpdateSwiperPicList()
    setUpdateSwiper()
    initUpdateSwiper()
}

//make week swiper
async function weekSwiper() {
    await getWeekSwiperPicList()
    setWeekSwiper();
    initWeekSwiper()
}

//make season swiper
async function seasonSwiper() {
    await getSeasonSwiperPicList()
    setSeasonSwiper()
    initSeasonSwiper()
}


//run main
main()



//latest update carousel
async function getUpdateSwiperPicList() {
    let res = await fetch("/series")
    let galleryQuery = await res.json()
    for (let elem of galleryQuery) {
        updateGalleryList.push(elem)
    }
}



// week favorite carousel
async function getWeekSwiperPicList() {
    let res = await fetch("/week-series")
    let galleryQuery = await res.json()
    for (let elem of galleryQuery) {

        weekGalleryList.push(elem)
    }
}

// season favorite carousel
async function getSeasonSwiperPicList() {
    let res = await fetch("/season-series")
    let galleryQuery = await res.json()
    for (let elem of galleryQuery) {
        seasonGalleryList.push(elem)
    }
}

//set update swiper
function setUpdateSwiper() {
    updateGalleryList.forEach((series) => {
        let sliderDiv = document.createElement("div");
        let titleDiv = document.createElement("div")
        let pictureImg = document.createElement("img");
        sliderDiv.className = `swiper-slide`;
        titleDiv.className = `series-title class-id="${series.id}"`
        titleDiv.innerHTML = series.title
        pictureImg.src = series.picture_path;
        sliderDiv.appendChild(pictureImg);
        sliderDiv.appendChild(titleDiv);
        updateSwiperWrapperElem.appendChild(sliderDiv)
    })
}


//set week swiper
function setWeekSwiper() {
    weekGalleryList.forEach((series) => {
        let sliderDiv = document.createElement("div");
        let titleDiv = document.createElement("div")
        let pictureImg = document.createElement("img");
        sliderDiv.className = `swiper-slide`;
        titleDiv.className = `series-title class-id="${series.id}"`
        titleDiv.innerHTML = series.title
        pictureImg.src = series.picture_path;
        sliderDiv.appendChild(pictureImg);
        sliderDiv.appendChild(titleDiv);
        weekSwiperWrapperElem.appendChild(sliderDiv)


    });;
};

//set season swiper
function setSeasonSwiper() {
    console.log(seasonGalleryList);
    seasonGalleryList.forEach((series) => {
        let sliderDiv = document.createElement("div");
        let titleDiv = document.createElement("div")
        let pictureImg = document.createElement("img");
        sliderDiv.className = `swiper-slide id:${series.id}`;
        titleDiv.className = `series-title id:${series.id}`
        titleDiv.innerHTML = series.title
        pictureImg.src = series.picture_path;
        sliderDiv.appendChild(pictureImg);
        sliderDiv.appendChild(titleDiv);
        seasonSwiperWrapperElem.appendChild(sliderDiv)
    });
}


//init update swiper
function initUpdateSwiper() {
    let swiper = new Swiper(".update-Swiper", {
        effect: "coverflow",
        grabCursor: false,
        centeredSlides: true,
        slidesPerView: "3",
        slideToClickedSlide: true,
        coverflowEffect: {
            rotate: 5,
            stretch: 50,
            depth: 200,
            modifier: 1,
            slideShadows: false
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        }
    })
    swiper.on('click', function() {
        let clickedSlide = swiper.clickedSlide
        if (clickedSlide.classList.contains("swiper-slide-active")) {
            window.location.href = `content.html?id=${updateGalleryList[swiper.clickedIndex].id}`
        } else if (clickedSlide.classList.contains("swiper-slide-next")) {
            swiper.slideNext()
        } else if (clickedSlide.classList.contains("swiper-slide-prev")) {
            swiper.slidePrev()
        }
    })
}


//init week swiper
function initWeekSwiper() {
    let swiper = new Swiper(".week-Swiper", {
        grabCursor: true,
        slidesPerView: 4,
        spaceBetween: 10,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
    });
    //go to content when click on slide
    swiper.on('click', function() {
        window.location.href = `content.html?id=${weekGalleryList[swiper.clickedIndex].id}`
    })

}

//init season swiper
function initSeasonSwiper() {
    let swiper = new Swiper(".season-Swiper", {
        grabCursor: true,
        slidesPerView: 4,
        spaceBetween: 30,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
    });
    //go to content when click on slide
    swiper.on('click', function() {

        window.location.href = `content.html?id=${seasonGalleryList[swiper.clickedIndex].id}`
    })
}