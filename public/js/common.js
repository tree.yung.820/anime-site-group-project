let loginBtnElem = document.querySelector(".login-btn");
let logoutBtnElem = document.querySelector(".logout-btn");
let userDropDownElem = document.querySelector(".user-dropdown");
let searchBtnElem = document.querySelector(".search-btn");
let watchHistoryBtnElem = document.querySelector(".watch-history");
let bookmarkBtnElem = document.querySelector(".bookmark");
let changeThemeBtnElem = document.querySelector(".change-theme");
let navbarElem = document.querySelector(".navbar");
let dropdownMenuElem = document.querySelector(".dropdown-menu");
let siteLogoElem = document.querySelector(".site-logo");
let bodyElem = document.querySelector("body");
let isLightMode = true;
export let userInfo;

main();

logoutBtnElem.onclick = logOut;
watchHistoryBtnElem.onclick = gotoHistoryPage;
bookmarkBtnElem.onclick = gotoBookmarkPage;
changeThemeBtnElem.onclick = changeTheme;

async function main() {
	userInfo = await getUser();
	if (userInfo.userInfo) {
		let userData = userInfo.userInfo;
		isLightMode = userInfo.mode;
		defaultTheme(isLightMode);
		document.body.classList.add("user-role");
		userInfo.userInfo.isAdmin && document.body.classList.add("admin-role");
		console.log("user: ", userData);
		let dropDownPic = document.querySelector(".user-info-pic");
		let profilePic = document.querySelector(".user-profile-pic");
		let getUser = document.querySelector(".user-info");
		let getUsername = document.querySelector(".user-name");
		dropDownPic.src = userData.avatarPath;
		getUsername.innerText = userData.username;
		profilePic.src = userData.avatarPath;
		loginBtnElem && loginBtnElem.setAttribute("hidden", "");
		userDropDownElem.removeAttribute("hidden");
	}
}

export async function getUser() {
	let res = await fetch("/user");
	return await res.json();
}

document.addEventListener("visibilitychange", e => {
    if (document.visibilityState === "hidden") {
        exitSession();
    }
});

async function logOut() {
	let res = await fetch("/logout", { method: "POST" });
	if (res.ok) {
		// document.body.classList.remove("user-role");
		// loginBtnElem.removeAttribute("hidden");
		// userDropDownElem.setAttribute("hidden", "");
		window.location.href = `index.html`;
	}
}

function gotoHistoryPage() {
	window.location.href = `history.html`;
}

function gotoBookmarkPage() {
	window.location.href = `bookmark.html`;
}

function changeTheme() {
	//check whether it is light or dark mode
	if (isLightMode) {
		changeToDarkMode();
	} else {
		changeToLightMode();
	}
}

function changeToDarkMode() {
	navbarElem.classList.add("dark");
	dropdownMenuElem.classList.add("dropdown-menu-dark");
	bodyElem.classList.add("dark");
	siteLogoElem.src = "/pictures/WAIFU_LOGO_Dark.png";
	isLightMode = false;
}

function changeToLightMode() {
	navbarElem.classList.remove("dark");
	dropdownMenuElem.classList.remove("dropdown-menu-dark");
	bodyElem.classList.remove("dark");
	siteLogoElem.src = "/pictures/WAIFU_LOGO_Light.png";
	isLightMode = true;
}

function defaultTheme(mode) {
	mode ? changeToLightMode() : changeToDarkMode();
}

async function exitSession() {
	let res = await fetch("/mode", {
		method: "POST",
		headers: { "Content-type": "application/json; charset=utf-8" },
		body: JSON.stringify({ lightMode: isLightMode }),
	});
}
// searchBtnElem.addEventListener("click", () => {

// })

// // search bar function
// async function searchEngine() {
//     console.log("search");
//     let res = await fetch("/search")
//     let searchQuery = await res.json()
//     console.log(searchQuery);
// }
