const galleryControlsContainer = document.querySelector(".gallery-controls");
const galleryControls = ["previous", "add", "next"];
const galleryContainer = document.querySelector(".gallery-container");
const galleryTitle = document.querySelector(".coverflow-title")
let galleryItemsElem
let seriesTitleElem
let galleryList = []


//add comments below to use this coverFlow

// <div class="gallery">
//             <div class="gallery-container">
//             </div>
//             <div class="coverflow-title"></div>
//         </div>


//<script src="js/coverflow.js"></script>


//get all series info and push to galleryList
const getSeriesPicList = async() => {
    let res = await fetch("/series")
    galleryQuery = await res.json()
    for (let elem of galleryQuery) {
        galleryList.push(elem)
    }
}

// insert series picture to coverflow and set logic to trigger setCurrentState
const setCarousel = () => {
    //set picture
    galleryList.forEach((image, i) => {
        //create title div
        let title = document.createElement("div")
            //create picture img
        let picture = document.createElement("img");
        //set picture onclick logic
        picture.onclick = () => {
                if (picture.classList.contains("gallery-item-3")) {

                    setCurrentState(3, image.id)
                    setCurrentTitle(3, image.id)

                } else if (picture.classList.contains("gallery-item-2")) {
                    setCurrentState(2)
                    setCurrentTitle(2)
                } else if (picture.classList.contains("gallery-item-4")) {
                    setCurrentState(4)
                    setCurrentTitle(4)
                } else if (picture.classList.contains("gallery-item-1")) {
                    setCurrentState(1)
                    setCurrentTitle(1)
                } else if (picture.classList.contains("gallery-item-5")) {

                    setCurrentState(5)
                    setCurrentTitle(5)

                }
            }
            //insert picture_path to img.src
        picture.src = image.picture_path;
        //insert class to img
        picture.className = `gallery-item gallery-item-${i + 1} data-index="${i + 1}" class-id="${image.id}"`;
        //insert picture to .gallery-container
        galleryContainer.appendChild(picture);

        title.innerText = image.title
        title.className = `gallery-title gallery-title-${i+1}`
        galleryTitle.appendChild(title)

    });


};


// Update the current order of the coverflow and set click to the content page
const setCurrentState = (direction, id) => {

    galleryItemsElem.forEach((el, i) => {
        el.classList.remove(`gallery-item-${i + 1}`);
    });

    switch (direction) {
        case 1:
            galleryItemsElem.unshift(galleryItemsElem.pop());
            galleryItemsElem.unshift(galleryItemsElem.pop());
            break;
        case 2:

            galleryItemsElem.unshift(galleryItemsElem.pop());
            break;
        case 3:

            window.location.href = `content.html?id=${id}`
            break;
        case 4:

            galleryItemsElem.push(galleryItemsElem.shift());

            break;
        case 5:


            galleryItemsElem.push(galleryItemsElem.shift());
            galleryItemsElem.push(galleryItemsElem.shift());
            break;
        default:
            break;
    }

    galleryItemsElem.forEach((el, i) => {
        el.classList.add(`gallery-item-${i + 1}`);
    });


};


function setCurrentTitle(direction, id) {
    seriesTitleElem.forEach((el, i) => {
        el.classList.remove(`gallery-title-${i+1}`)
    })
    switch (direction) {
        case 1:
            seriesTitleElem.unshift(seriesTitleElem.pop());
            seriesTitleElem.unshift(seriesTitleElem.pop());
            break;
        case 2:
            seriesTitleElem.unshift(seriesTitleElem.pop());
            break;
        case 3:
            window.location.href = `content.html?id=${id}`
            break;
        case 4:
            seriesTitleElem.push(seriesTitleElem.shift());

            break;
        case 5:
            seriesTitleElem.push(seriesTitleElem.shift());
            seriesTitleElem.push(seriesTitleElem.shift());

            break;
        default:
            break;
    }
    seriesTitleElem.forEach((el, i) => {
        el.classList.add(`gallery-title-${i + 1}`);
    });


}


async function main() {
    await getSeriesPicList()
    setCarousel();
    const galleryItems = document.querySelectorAll(".gallery-item");
    const seriesTitle = document.querySelectorAll(".gallery-title")
    galleryItemsElem = Array.from(galleryItems);
    seriesTitleElem = Array.from(seriesTitle)
}
main()