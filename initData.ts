import pg from "pg";
import xlsx from "xlsx";
import env from "./lib/env";
import { hashPassword } from "./lib/hash";
import {
	UserData,
	SeriesData,
	EpisodeData,
	TypeData,
	DanmakuData,
	FavoriteListData,
	UserEpisodeData,
	CommentData,
} from "./lib/interface";

export const client = new pg.Client({
	database: env.DB_NAME,
	user: env.DB_USER,
	password: env.DB_PASSWORD,
});

//init db data
const xlsxDataDefault = async () => {
	await client.connect();
	//read xlsx file
	let workbook = xlsx.readFile("project_database_info.xlsx");

	let userData: UserData[] = xlsx.utils.sheet_to_json(workbook.Sheets["users"]);
	let seriesData: SeriesData[] = xlsx.utils.sheet_to_json(workbook.Sheets["series"]);
	let episodeData: EpisodeData[] = xlsx.utils.sheet_to_json(workbook.Sheets["episodes"]);
	let typeData: TypeData[] = xlsx.utils.sheet_to_json(workbook.Sheets["type"]);
	let danmakuData: DanmakuData[] = xlsx.utils.sheet_to_json(workbook.Sheets["danmakus"]);
	let userEpisodeData: UserEpisodeData[] = xlsx.utils.sheet_to_json(workbook.Sheets["user_episode"]);
	let favoriteListData: FavoriteListData[] = xlsx.utils.sheet_to_json(workbook.Sheets["favorite_lists"]);
	let commentData: CommentData[] = xlsx.utils.sheet_to_json(workbook.Sheets["comments"]);

	await client.query("DELETE FROM episode_history")
	await client.query("DELETE FROM comments")
	await client.query("DELETE FROM user_episode");
	await client.query("DELETE FROM favorite_lists");
	await client.query("DELETE FROM danmakus");
	await client.query("DELETE from users");
	await client.query("ALTER SEQUENCE users_id_seq RESTART WITH 1");
	await client.query("DELETE from episodes");
	await client.query("ALTER SEQUENCE episodes_id_seq RESTART WITH 1");
	await client.query("DELETE from series");
	await client.query("ALTER SEQUENCE series_id_seq RESTART WITH 1");
	await client.query("DELETE from types");
	await client.query("ALTER SEQUENCE types_id_seq RESTART WITH 1");

	//insert series data
	for (let series of seriesData) {
		await client.query(
			/*sql*/
			`INSERT into series
            (title, update_on, description, picture_path, created_at,updated_at) 
			values ($1,$2,$3,$4,NOW(),NOW())`,
			[series.title, series.updateOn, series.description, series.picturePath]
		);
	}
	// insert episodes data
	for (let episode of episodeData) {
		let result = await client.query(
			/*sql*/
			`SELECT id FROM series WHERE title = $1`,
			[episode.seriesTitle]
		);

		let seriesId = result.rows[0].id;

		await client.query(
			/*sql*/
			`INSERT into episodes(
			series_id, episode_number, title, local_path, external_link, created_at, updated_at)
			values($1, $2, $3, $4, $5, NOW(), NOW())`,
			[seriesId, episode.episodeNumber, episode.title, episode.localPath, episode.externalLink]
		);
	}
	//insert user data
	for (let user of userData) {
		let hashedPassword = await hashPassword(user.password.toString());
		await client.query(
			/*sql*/
			`INSERT INTO users
			(email,password,username,avatar_path,is_admin,created_at,updated_at) 
			values ($1,$2,$3,$4,$5,NOW(),NOW())`,
			[user.email, hashedPassword, user.username, user.avatarPath, user.admin]
		);
	}
	//insert types data
	for (let type of typeData) {
		await client.query(
			/*sql*/
			`INSERT INTO types (name) values ($1)`,
			[type.name]
		);
	}
	// insert danmaku data
	for (let danmaku of danmakuData) {
		await client.query(
			/*sql*/
			`INSERT INTO danmakus
				(user_id, episode_id, content, mode, color, time, created_at)
				values ($1, $2, $3, $4, $5, $6, NOW())`,
			[danmaku.userId, danmaku.episodeId, danmaku.content, danmaku.mode, danmaku.color, danmaku.time]
		);
	}

	// insert user_episode
	for (let userEpisode of userEpisodeData) {
		await client.query(
			/*sql*/
			`INSERT INTO user_episode
			(user_id, episode_id, last_watched, like_status, created_at, updated_at)
			values ($1, $2, $3, $4, NOW(), NOW())`,
			[userEpisode.userId, userEpisode.episodeId, userEpisode.time, userEpisode.likeStatus]
		);
	}
	// insert favorite_lists
	for (let favoriteList of favoriteListData) {
		await client.query(
			/*sql*/
			`INSERT INTO favorite_lists
			(user_id, series_id, episode_id, bookmark, created_at, updated_at)
			values ($1, $2, $3, $4, NOW(), NOW())`,
			[favoriteList.userId, favoriteList.seriesId, favoriteList.episodeId, favoriteList.bookmark]
		);
	}
	// insert comment data
	for (let comment of commentData) {
		await client.query(
			/*sql*/ `INSERT INTO comments
			(user_id, series_id, episode_id, content, created_at, updated_at)
			values ($1, $2, $3, $4, NOW(), NOW())`,
			[comment.userId, comment.seriesId, comment.episodeId, comment.content]
		);
	}
	await client.end();
};

xlsxDataDefault();
