import "/js/common.js";
import { convertToLocalDate } from "/js/lib/helper.js";

const viewBookmarkElem = document.querySelector(".view-history-container");
let viewBookmarkList = [];
let innerArray = [];

async function getUserBookmark() {
    let res = await fetch("/user/history");
    let history = await res.json();
    getViewBookmarkGrid(history);
}

//set grid
async function getViewBookmarkGrid(history) {
    let historyArray = history.rows;
    console.log("history: ", historyArray);
    historyArray.forEach((result) => {
        if (result.bookmark == true) {
            viewBookmarkList.push(result);
        }
    });
}

async function setBookmarkGrid() {
    viewBookmarkList.forEach((history) => {
        let historyGridHTMLString = `
        <div class="history-grid">
        <a href="content.html?id=${history.id}"><div class="history-inner-wrap ${history.id}">
        <img src="${history.picture_path}" alt="" class="series-pic">
            <div class="details">
                <div class="series-title">${history.title}</div>
                    <div class="epi-watch-at">
                        <div class="episode">第${history.episode_number}集</div>
                        <div class="watch-at">上次觀看 : ${convertToLocalDate(history.updated_at)}</div>
                    </div>
            </div>
    </div></a>
    </div>
    `;
        viewBookmarkElem.innerHTML += historyGridHTMLString;
    });
}

async function setEvent() {
    console.log("viewBookList: ", viewBookmarkList);
    let innerWrapNodes = document.querySelectorAll(".history-inner-wrap");
    console.log("innerWrapArray: ", innerWrapNodes);
    innerArray = Array.from(innerWrapNodes);
    console.log(innerArray);
    innerArray.forEach((elem) => {
        elem.onclick = () => {
            window.location.href = `content.html?id=${elem.id}`;
        };
    });
    // innerWrapArray.forEach(wrap => {
    //     wrap.onclick =
    // })
}

await getUserBookmark();
await setBookmarkGrid();
await setEvent();
