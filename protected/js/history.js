import "/js/common.js";
import { convertToLocalDate } from "/js/lib/helper.js";

const viewHistoryElem = document.querySelector(".view-history-container");
let viewHistoryList = [];
let innerArray = []

//load user history
async function getUserHistory() {
    let res = await fetch("/user/history");
    let history = await res.json();
    getViewHistoryGrid(history);

}

//set grid
async function getViewHistoryGrid(history) {
    let historyArray = history.rows;
    historyArray.forEach(result => {
        viewHistoryList.push(result);
    });
}

async function setGrid() {
    viewHistoryList.forEach(history => {
        let historyGridHTMLString = `
        <div class="history-grid">
    <a href="content.html?id=${history.id}"><div class="history-inner-wrap">
        <img src="${history.picture_path}" alt="" class="series-pic">
            <div class="details">
                <div class="series-title">${history.title}</div>
                    <div class="epi-watch-at">
                        <div class="episode">第${history.episode_number}集</div>
                        <div class="watch-at">上次觀看 : ${convertToLocalDate(history.updated_at)}</div>
                    </div>
            </div>
    </div></a>
    </div>
    `;
        viewHistoryElem.innerHTML += historyGridHTMLString;
    });
}

// async function setEvent() {
//     let innerWrapNodes = document.querySelectorAll(".history-inner-wrap")
//     console.log("innerWrapArray: ", innerWrapNodes);
//     innerArray = Array.from(innerWrapNodes)
//     console.log("innerArray: ", innerArray);
//     innerArray.forEach(elem => {
//             console.log("history: ", elem);
//             elem.onclick = () => {

//             }
//         })
//         // innerWrapArray.forEach(wrap => {
//         //     wrap.onclick = 
//         // })
// }


await getUserHistory();
await setGrid();
// await setEvent()