import { errorMessage, previewPic } from "/js/lib/helper.js";
import "/js/common.js";
import { getUser } from "/js/common.js";

let avatarPreview = document.querySelector("#avatar-preview");
let avatarInput = document.querySelector("#avatarInput");
let settingFormElem = document.querySelector("#setting-form");
let userInfo = await getUser();
let originalPic = (avatarPreview.src = userInfo.userInfo.avatarPath);

// avatarInput.onchange = (e) => previewPic(e, originalPic);
settingFormElem.addEventListener("submit", async (e) => {
    e.preventDefault();
    console.log("setting clicked");
    let form = e.target;

    let formObj = {
        email: "", //for validate json not the bust
        username: form.username.value,
        password: form.password.value,
        confirmPassword: form.confirmPassword.value,
    };

    console.log(formObj);
    const res = await fetch("/validate", {
        method: "POST",
        headers: { "Content-Type": "application/json; charset=utf-8" },
        body: JSON.stringify(formObj),
    });

    let result = await res.json();
    console.log(result);

    if (res.ok) {
        let res2 = await fetch("/user", {
            method: "PUT",
            headers: { "Content-Type": "application/json; charset=utf-8" },
            body: JSON.stringify(formObj),
        });
    }
});
