import express from "express";
import http from "http";
import { Server as SocketIO } from "socket.io";
import { setSocketIO } from "./lib/socket-io";
import sessionMiddleWare from "./lib/session";
import env from "./lib/env";
import oAuthMiddleWare from "./lib/oauth";
import seriesRoutes from "./routes/series-routes";
import episodeRoutes from "./routes/episode-routes";
import userRoutes from "./routes/user-routes";
import danmakuRoutes from "./routes/danmaku-routes";
import userEpisodeRoutes from "./routes/user-episode-routes";
import favoriteRoutes from "./routes/favorite-routes";
import commentRoutes from "./routes/comment-routes";
import epHistoryRoutes from "./routes/episode-history-routes";
import { isLoggedIn } from "./lib/guard";

const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);
setSocketIO(io);

app.use(express.json());
app.use(sessionMiddleWare);
app.use(oAuthMiddleWare);
app.use(express.static("assets"));
app.use(express.static("assets/pictures/test_index_pic"));
app.use(express.static("assets/uploads"));
app.use(seriesRoutes);
app.use(episodeRoutes);
app.use(commentRoutes);
app.use(danmakuRoutes);
app.use(userRoutes);
app.use(userEpisodeRoutes);
app.use(epHistoryRoutes);
app.use(favoriteRoutes);
app.use(express.static("public"));
app.use(isLoggedIn, express.static("protected"));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const PORT = env.PORT;

server.listen(PORT, () => {
    console.log(`Listening at port ${PORT}`);
});
