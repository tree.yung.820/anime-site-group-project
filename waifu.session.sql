CREATE TABLE series(
    id SERIAL primary key,
    title VARCHAR(255),
    update_on SMALLINT,
    description TEXT,
    picture_path TEXT,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE
);
CREATE TABLE types(
    id SERIAL primary key,
    name VARCHAR(100)
);
CREATE TABLE series_types(
    series_id INTEGER,
    FOREIGN KEY (series_id) REFERENCES series(id),
    types_id INTEGER,
    FOREIGN KEY (types_id) REFERENCES types(id)
);
CREATE TABLE episodes(
    id SERIAL primary key,
    series_id INTEGER,
    FOREIGN KEY (series_id) REFERENCES series(id),
    episode_number INTEGER,
    title TEXT,
    local_path TEXT,
    external_link TEXT,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE
);
CREATE TABLE users(
    id SERIAL primary key,
    username VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255),
    avatar_path VARCHAR(255),
    is_admin BOOLEAN,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE
);
CREATE TABLE user_episode(
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    episode_id INTEGER,
    FOREIGN KEY (episode_id) REFERENCES episodes(id),
    last_watched INTEGER,
    like_status TEXT,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE
);
CREATE TABLE episode_history(
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    episode_id INTEGER,
    FOREIGN KEY (episode_id) REFERENCES episodes(id),
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE
);
CREATE TABLE danmakus(
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    episode_id INTEGER,
    FOREIGN KEY (episode_id) REFERENCES episodes(id),
    content VARCHAR(255),
    mode VARCHAR(255),
    color VARCHAR(255),
    time DECIMAL,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE
);
CREATE TABLE comments(
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    series_id INTEGER,
    FOREIGN KEY (series_id) REFERENCES series(id),
    episode_id INTEGER,
    FOREIGN KEY (episode_id) REFERENCES episodes(id),
    content TEXT,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE
);
CREATE TABLE favorite_lists(
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    series_id INTEGER,
    FOREIGN KEY (series_id) REFERENCES series(id),
    episode_id INTEGER,
    FOREIGN KEY (episode_id) REFERENCES episodes(id),
    bookmark BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE
);
SELECT *
FROM series;
SELECT *
FROM types;
SELECT *
FROM series_types;
SELECT *
FROM episodes;
SELECT episode_id
FROM episode_history
WHERE episode_history.created_at > CURRENT_DATE - INTERVAL '7 days';