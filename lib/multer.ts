import multer from "multer";

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "./assets/uploads");
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${file.originalname.split(".")[0]}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});
const storageWithFileName = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "/pictures");
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${file.originalname.split(".")[0]}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});
const upload = multer({ storage });
const uploadWithFileName = multer({ storage: storageWithFileName });

export const multerSingleImage = upload.single("image");
export const multerSingleImageProfile = uploadWithFileName.single("profile");
