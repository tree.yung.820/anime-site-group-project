import camelcaseKeys from "camelcase-keys";

export function createResultJSON(data: any | undefined = {}, err: any | undefined = {}) {
	return {
		data,
		err,
	};
}

export function camelCaseKeys(obj: any) {
	return camelcaseKeys(obj);
}