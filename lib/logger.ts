import { createLogger, format, transports } from "winston";
import env from "./env";

const logFormat = format.printf(function (info) {
    let date = new Date().toISOString();
    return `${date}[${info.level}]: ${info.message}`;
});

export const logger = createLogger({
    level: env.WINSTON_LEVEL,
    format: format.combine(format.colorize(), logFormat),
    transports: [new transports.Console()],
});
