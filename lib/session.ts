import session from "express-session";
import env from "./env";


const sessionMiddleWare = session({
    secret: env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
})

export default sessionMiddleWare