//user interface for xlsx

export interface SeriesData {
	title: string;
	updateOn: string;
	description: string;
	picturePath: string;
}

export interface EpisodeData {
	seriesTitle: string;
	episodeNumber: number;
	title: string;
	localPath?: string;
	externalLink?: string;
}

export interface UserData {
	email: string;
	password: string;
	username: string;
	avatarPath?: string;
	admin: boolean;
}

export interface TypeData {
	name: string;
}

export interface DanmakuData {
	userId: number;
	episodeId: number;
	content: string;
	mode: string;
	time: string;
	color: string;
}

export interface UserEpisodeData {
	userId: number;
	episodeId: number;
	time: number;
	likeStatus?: string;
}

export interface FavoriteListData {
	userId: number;
	seriesId: number;
	episodeId: number;
	bookmark?: boolean;
}

export interface CommentData {
	userId: number;
	seriesId: number;
	episodeId: number;
	content: string;
}

export type WatchInfo = Omit<UserEpisodeData, "userId" | "episodeId"> & { epId: number };

export type LikeStatus = {
	likeStatus: string;
	count: number;
};
