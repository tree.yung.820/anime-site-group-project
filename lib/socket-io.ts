import socketIO from "socket.io";
import { logger } from "./logger";

export let io: socketIO.Server;

export function setSocketIO(value: socketIO.Server) {
	io = value;

	io.on("connection", socket => {
		logger.info(`${socket.id} is online.`);

		socket.on("join-room", roomId => {
			socket.join(roomId.toString());
			logger.info(`socket ${socket.id} has joined room ${roomId}.`);
		});

		socket.on("leave-room", roomId => {
			socket.leave(roomId.toString());
			logger.info(`socket ${socket.id} has left room ${roomId}.`);
		});

		socket.on("delete-danmaku", roomId => {
			setInterval(() => {
				socket.broadcast.emit("delete", "del");
			}, 100);
		});
	});
}
