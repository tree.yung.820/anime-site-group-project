import dotenv from "dotenv";

dotenv.config();

const env = {
    DB_NAME: process.env.DB_NAME || "anime",
    DB_USER: process.env.DB_USER || "postgres",
    DB_PASSWORD: process.env.DB_PASSWORD || "postgres",
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    SESSION_SECRET: process.env.SESSION_SECRET || "waifu sekaiichi",
    WINSTON_LEVEL: process.env.WINSTON_LEVEL || "info",
    PORT: process.env.PORT || 8080,
};

export default env;
