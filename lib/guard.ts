import { Request, Response, NextFunction } from "express";
import { createResultJSON } from "./helper";

export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    console.log("Req : ", req.path);

    if (req.session["user"]) {
        next();
    } else {
        res.json(createResultJSON(null, { message: "Not Logged in" }));
    }
}

export function isAdmin(req: Request, res: Response, next: NextFunction) {
    if (req.session["user"].isAdmin) {
        next();
    } else {
        res.json(createResultJSON(null, { message: "Not admin" }));
    }
}
